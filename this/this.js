/** 4 варианта исправления */

const userService = {
    currentFilter: 'active',
    users: [
        { name: 'Alex', status: 'active' },
        { name: 'Nick', status: 'deleted' }
    ],
    getFilteredUsers: function() {
        return this.users.filter(function (user) {
            return user.status === 'this.currentFilter';
        });
    }
}

console.log(userService.getFilteredUsers());

/** 1 вариант решения */

const userService = {
    currentFilter: 'active',
    users: [
        { name: 'Alex', status: 'active' },
        { name: 'Nick', status: 'deleted' }
    ],
    getFilteredUsers: function() {
        return this.users.filter((user) => {
            return user.status === this.currentFilter;
        });
    }
}

console.log(userService.getFilteredUsers());

/** 2 вариант решения */

const userService = {
    currentFilter: 'active',
    users: [
        { name: 'Alex', status: 'active' },
        { name: 'Nick', status: 'deleted' }
    ],
    getFilteredUsers: function() {
        return this.users.filter(function(user) {
            return user.status === this.currentFilter;
        }, this);
    }
}

console.log(userService.getFilteredUsers());

/** 3 вариант решения */

const userService = {
    currentFilter: 'active',
    users: [
        { name: 'Alex', status: 'active' },
        { name: 'Nick', status: 'deleted' }
    ],
    getFilteredUsers: function() {
        return this.users.filter(function(user) {
            return user.status === this.currentFilter;
        }.bind(this));
    }
}

console.log(userService.getFilteredUsers());

/** 4 вариант решения */

const userService = {
    currentFilter: 'active',
    users: [
        { name: 'Alex', status: 'active' },
        { name: 'Nick', status: 'deleted' }
    ],
    getFilteredUsers: function() {
        const that = this;
        
        return that.users.filter(function(user) {
            return user.status === that.currentFilter;
        });
    }
}

console.log(userService.getFilteredUsers());