/** реализовать класс EventEmitter*/

class EventEmitter {}

const ee = new EventEmitter();

const cb1 = () => console.log('cb1');
const cb2 = () => console.log('cb2');

ee.on('event', cb1);
ee.on('event', cb2);

ee.emit('event');
// cb1
// cb2

ee.off('event', cb2);

ee.emit('event');
// cb1


/** реализация класса 8*/

class EventEmitter {
    events = {};

    on(eventName, cb) {
        if (this.events[eventName]) {
            this.events[eventName].push(cb);
        } else {
            this.events[eventName] = [cb];
        }
    }

    emit(eventName) {
        if (this.events[eventName]) {
            this.events[eventName].forEach(event => event());
        }
    }

    off(eventName, cb) {
        if (this.events[eventName]) {
            this.events[eventName] = this.events[eventName].filter(event => event !== cb);
        }

        if (this.events[eventName] && !this.events[eventName].length) {
            delete this.events[eventName];
        }
    }
}